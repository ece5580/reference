# README #

### What is this repository for? ###

* This repository covers the executable specification for 
Group 8 and our project selection of Hash-Based Signature Systems and the 
MSP-430.  This will serve as a reference model to study how the cryptographic 
algorithm works and to generate test-vectors.

* Version
Report 1

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Michael Cantrell
* Kimberly Zeitz
* Caleb Stroud