#from __future__ import division
import hashlib
from bitstring import BitArray
from math import ceil, floor, log
import os


# Helper functions for ease of conversion

# Provide hexidecimal representation as a string without leading 0x
def Hex(number):
    if hex(number)[-1:] == 'L':
        return hex(number)[2:-1]
    else:
        return hex(number)[2:]

def lg(number):
    return log(number, 2)


# Take an m byte input and give an m byte hashed output
def sha256_20(value, m = 40):
    if len(value) != m:
        raise ValueError('Input to SHA256-20 (F) needs to be {} bytes and in hexdigest form' % m/2)
    else:
        hexlen = m
        return hashlib.sha256(value).hexdigest()[-hexlen:]

def sha256_16(value):
    return BitArray(hex=hashlib.sha256(value).hexdigest())[:8*16]

def byte(byte_string_hex, i):
    h = BitArray(hex=byte_string_hex)
    if i >= len(h.hex)/2:
        raise ValueError('Input value i must be less than the number of bytes in the byte string')
    else:
        return h[8*i:8*i+8]

def bytes(byte_string_hex, i, j):
    h = BitArray(hex=byte_string_hex)
    if i >= len(h.hex)/2:
        raise ValueError('Input value i must be less than the number of bytes in the byte string')
    if j >= len(h.hex)/2:
        raise ValueError('Input value j must be less than the number of bytes in the byte string')

    b = BitArray(hex=byte_string_hex)
    v = BitArray(bytes=b.bytes[i:j+1])
    return v

def coef(bit_string, i, w):
    if i >= len(bit_string)/w:
        raise ValueError('Input value i must be less than the number of bytes in the byte string')
    elif w not in [1, 2, 4, 8]:
        raise ValueError('Input value w must be one of the set {1, 2, 4, 8}')
    else:
        b = bit_string
        v = b[i*w:i*w+w]
        return v

# u = 8*n/w
def cksm(S, n, w, ls):
    summation = 0
    u = int(ceil(float(8)*n/float(w)))
    for i in range(u):
        summation = summation + (2**w - 1) - coef(S, i, w).uint
    s = BitArray('uint:16='+str(summation))
    return s << ls


def gen_priv_key(n, p):
    x = []
    for i in range(p):
        x.append(BitArray(bytes=os.urandom(n)))
    return x

def gen_pub_key(priv_key, H, w, p, I, q):
    y = []
    d_iter = BitArray(hex='0x00')
    d_pblc = BitArray(hex='0x01')
    for i in range(p):
        tmp = priv_key[i]
        i_network = BitArray('uintbe:16='+str(i))
        for j in range(2**w - 1):
            j_byte = BitArray('uint:8='+str(j))
            tmp.append(I)
            tmp.append(q)
            tmp.append(i_network)
            tmp.append(j_byte)
            tmp.append(d_iter)
            tmp = BitArray(hex=H(tmp.bytes).hexdigest())
        y.append(tmp)

    byte_string = I
    byte_string.append(q)
    for element in y:
        byte_string.append(element)
    byte_string.append(d_pblc)
    return BitArray(hex=H(byte_string.bytes).hexdigest())


def gen_signature(message, priv_key, H, I, q, p, n, w, ls):
    y = []
    C = BitArray(bytes=os.urandom(n))
    ots_type = BitArray('uint:32=1')
    message.append(C)
    message.append(I)
    message.append(q)
    message.append('0x02')
    Q = BitArray(hex=H(message.bytes).hexdigest())
    for i in range(p):
        t = Q
        t.append(cksm(Q, n, w, ls))
        a = coef(t, i, w)
        tmp = priv_key[i]
        for j in range(a.uint):
            tmp.append(I)
            tmp.append(q)
            tmp.append(BitArray('uintbe:16='+str(i)))
            tmp.append(BitArray('uint:8='+str(j)))
            tmp.append('0x00')
            tmp = BitArray(hex=H(tmp.bytes).hexdigest())
        y.append(tmp)
    byte_string = ots_type
    byte_string.append(C)
    byte_string.append(I)
    byte_string.append('0x00')
    byte_string.append(q)
    for element in y:
        byte_string.append(element)
    return byte_string

def verify_signature(message, signature, public_key, H, n, p, w, ls):
    sig_stream = ConstBitStream(signature)
    ots_type = BitArray(bytes=sig_stream.read('bytes:4'))
    C = BitArray(bytes=sig_stream.read('bytes:'+n))
    I = BitArray(bytes=sig_stream.read('bytes:31'))
    NULL = BitArray(bytes=sig_stream.read('bytes:1'))
    q = BitArray(bytes=sig_stream.read('bytes:4'))
    d_mesg = BitArray('0x02')
    y = []
    for i in range(p):
        y.append(BitArray(bytes=sig_stream.read('bytes:'+n)))

    Q_hash = message
    message.append(C)
    message.append(I)
    message.append(q)
    message.append(d_mesg)

    Q = BitArray(hex=H(Q_hash.bytes).hexdigest())
    z = []
    for i in range(p):
        t = Q
        t.append(cksm(Q, n, w, ls))
        a = (2**w - 1) - coef(t, i, w).uint
        tmp = y[i]
        for j in range(a+1, 2**w - 1):
            tmp.append(I)
            tmp.append(q)
            tmp.append(BitArray('uintbe:16='+str(i)))
            tmp.append(BitArray('uint:8='+str(j)))
            tmp.appen('0x00')
            tmp = BitArray(hex=H(tmp.bytes).hexdigest())
        z.append(tmp)
    cand_tmp = BitArray()
    for element in z:
        cand_tmp.append(element)
    cand_tmp.append(I)
    cand_tmp.append(q)
    cand_tmp.append('0x01')
    candidate = BitArray(hex=H(cand_tmp.bytes).hexdigest())

    if candidate == public_key:
        return True
    else:
        return False
