
import hashlib
from math import ceil, floor, log
import os

# LMOTS Constants
D_ITER = chr(0x00)
D_PBLC = chr(0x01)
D_MESG = chr(0x02)

# Null Byte
NULL = chr(0)

# Type Codes
lmots_sha256_n16_w1 = 0x00000001
lmots_sha256_n16_w2 = 0x00000002
lmots_sha256_n16_w4 = 0x00000003
lmots_sha256_n16_w8 = 0x00000004
lmots_sha256_n32_w1 = 0x00000005
lmots_sha256_n32_w2 = 0x00000006
lmots_sha256_n32_w4 = 0x00000007
lmots_sha256_n32_w8 = 0x00000008

# LMOTS Parameters
n = 32; p = 34; w = 8; ls = 0;


# SHA256 hash function
#   @params x - String to be hashed
def H(x):
    return hashlib.sha256(x).digest()[:n]

# StringToUint(S)
#   convert a byte string to a Uint
def StringToUint(S):
    sum = 0
    for c in S:
        sum = (sum << 8) | ord(c)
    return sum

def uint16ToString(x):
    b1 = chr(x & 0xff)
    x >>= 8
    b0 = chr(x & 0xff)
    return b0 + b1

def uint8ToString(x):
    return chr(x)


# Coef Function
#   @params S - Byte String (should always be n+2 bytes)
#   @params i - ith w-bit element of S
def coef(S, i):
    Sint = StringToUint(S)
    mask = (1<<w)-1
    print("Sint: {}".format(hex(Sint)))
    print("Mask: {}".format(hex(mask << ((p-i-1)*w))))
    value = (Sint & (mask << ((p-i-1)*w))) >> ((p-i-1)*w)
    return chr(value)

# Cksm Funtion
def cksm(S):
    sum = 0
    for i in range(u):
        sum = sum + (2**w - 1) - coef(S, i)
    return sum << ls

# Algorithm 0: Private Key Generation
def lmots_gen_priv():
    priv = []
    for i in range(p):
        priv.append(os.urandom(n))
    return priv

# Algorithm 1: Public Key Generation
def lmots_gen_pub(private_key, I, q):
    h = hashlib.sha256()
    h.update(I + q)
    for x in private_key:
        tmp = x
        for j in range(2**w):
            tmp = H(tmp + I + q + uint16ToString(i) + uint8ToString(j) + D_ITER)
        h.update(tmp)
    h.update(D_PBLC)
    return h.digest()




