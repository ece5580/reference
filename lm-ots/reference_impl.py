# example implementation for Leighton-Micali hash based signatures
# Internet draft
#
# Notes:
#
#     * only a limted set of parameters are supported; in particular,
#     * w=8 and n=32
#
#     * HLMS, LMS, and LM-OTS are all implemented
#
#     * uncommenting print statements may be useful for debugging, or
#       for understanding the mechanics of
#
#
# LMOTS constants
#
D_ITER = chr(0x00) # in the iterations of the LM-OTS algorithms
D_PBLC = chr(0x01) # when computing the hash of all of the iterates in the LM-OTS algorithm
D_MESG = chr(0x02) # when computing the hash of the message in the LMOTS algorithms
D_LEAF = chr(0x03) # when computing the hash of the leaf of an LMS tree
D_INTR = chr(0x04) # when computing the hash of an interior node of an LMS tree

NULL   = chr(0)    # used as padding for encoding

lmots_sha256_n32_w8 = 0x08000008 # typecode for LM-OTS with n=32, w=8
lms_sha256_n32_h10  = 0x02000002 # typecode for LMS with n=32, h=10
hlms_sha256_n32_l2  = 0x01000001 # typecode for two-level HLMS with n=32

# LMOTS parameters
#
n = 32; p = 34; w = 8; ls = 0

def bytes_in_lmots_sig():
    return n*(p+1)+40 # 4 + n + 31 + 1 + 4 + n*p

from Crypto.Hash import SHA256
from Crypto import Random
from bitstring import BitArray

# SHA256 hash function
#
def H(x):
#    print "hash input: " + stringToHex(x)
    h = SHA256.new()
    h.update(x)
    return h.digest()[0:n]

def sha256_iter(x, num):
    tmp = x
    for j in range(0, num):
        tmp = H(tmp + I + q + uint16ToString(i) + uint8ToString(j) + D_ITER)

# entropy source
#
entropySource = Random.new()

# integer to string conversion
#
def uint32ToString(x):
    c4 = chr(x & 0xff)
    x = x >> 8
    c3 = chr(x & 0xff)
    x = x >> 8
    c2 = chr(x & 0xff)
    x = x >> 8
    c1 = chr(x & 0xff)
    return c1 + c2 + c3 + c4

def uint16ToString(x):
    c2 = chr(x & 0xff)
    x = x >> 8
    c1 = chr(x & 0xff)
    return c1 + c2

def uint8ToString(x):
    return chr(x)

def stringToUint(x):
    sum = 0
    for c in x:
        sum = sum * 256 + ord(c)
    return sum

def coef(S, i):
    c = BitArray(bytes=S)
    v = c[w*i:w*i+w]
    return v.uint
    # c = bin(int(S,16))[2:]
    # v = c[w*i:w*i+w]
    # v = hex(int(v,2))
    # return v


# string-to-hex function needed for debugging
#
def stringToHex(x):
    return "".join("{:02x}".format(ord(c)) for c in x)

# LM-OTS functions
#
def encode_lmots_sig(C, I, q, y):
    result = uint32ToString(lmots_sha256_n32_w8) + C + I + NULL + q
    for i, e in enumerate(y):
        result = result + y[i]
    return result

def decode_lmots_sig(sig):
    if (len(sig) != bytes_in_lmots_sig()):
        print "error decoding signature; incorrect length (" + str(len(sig)) + " bytes)"
    typecode = sig[0:4]
    if (typecode != uint32ToString(lmots_sha256_n32_w8)):
        print "error decoding signature; got typecode " + stringToHex(typecode) + ", expected: " + stringToHex(uint32ToString(lmots_sha256_n32_w8))
        return ""
    C = sig[4:n+4]
    I = sig[n+4:n+35]
    q = sig[n+36:n+40] # note: skip over NULL
    y = list()
    pos = n+40
    for i in range(0, p):
        y.append(sig[pos:pos+n])
        pos = pos + n
    return C, I, q, y

def print_lmots_sig(sig):
    C, I, q, y = decode_lmots_sig(sig)
    print "C:\t" + stringToHex(C)
    print "I:\t" + stringToHex(I)
    print "q:\t" + stringToHex(q)
    for i, e in enumerate(y):
        print "y[" + str(i) + "]:\t" + stringToHex(e)

# Algorithm 0: Generating a Private Key
#
def lmots_gen_priv():
    priv = list()
    for i in range(0, p):
        priv.append(entropySource.read(n))
    return priv

# Algorithm 1: Generating a Public Key From a Private Key
#
def lmots_gen_pub(private_key, I, q):
    hash = SHA256.new()
    hash.update(I + q)
    for i, x in enumerate(private_key):
        tmp = x
        # print "i:" + str(i) + " range: " + str(range(0, 256))
        for j in range(0, 2**w):
            tmp = H(tmp + I + q + uint16ToString(i) + uint8ToString(j) + D_ITER)
        hash.update(tmp)
    hash.update(D_PBLC)
    return hash.digest()

# Algorithm 2: Merkle Checksum Calculation
# NEED TO UPDATE
def checksum(x):
    sum = 0
    for c in x:
        sum = sum + ord(c)
    # print format(sum, '04x')
    c1 = chr(sum >> 8)
    c2 = chr(sum & 0xff)
    return c1 + c2

# Algorithm 3: Generating a Signature From a Private Key and a Message
#
def lmots_gen_sig(private_key, I, q, message):
    C = entropySource.read(n)
    hashQ = H(message + C + I + q + D_MESG)
    # print "GEN hashQ: " + hashQ
    V = hashQ + checksum(hashQ)
    # print "V: " + stringToHex(V)
    y = list()
    for i, x in enumerate(private_key):
        # V = coef(hashQ + checksum(hashQ), i)
        # print "Gen V: " + str(V)
        tmp = x
        # print "i:" + str(i) + " range: " + str(range(0, ord(V[i])))
        for j in range(0, ord(V[i])):
            tmp = H(tmp + I + q + uint16ToString(i) + uint8ToString(j) + D_ITER)
        y.append(tmp)
    return encode_lmots_sig(C, I, q, y)

def lmots_sig_to_pub(sig, message):
    C, I, q, y = decode_lmots_sig(sig)
    hashQ = H(message + C + I + q + D_MESG)
    # print "Ver hashQ: " + hashQ
    V = hashQ + checksum(hashQ)
    # print "V: " + stringToHex(V)
    hash = SHA256.new()
    hash.update(I + q)
    for i, y in enumerate(y):
        # V = (2**w - 1) - coef(hashQ + checksum(hashQ), i)
        # V = coef(hashQ + checksum(hashQ), i)
        # print "Ver V: " + str(V)
        tmp = y
        # print "i:" + str(i) + " range: " + str(range(ord(V[i]), 256))
        for j in range(ord(V[i]), 2**w):
            tmp = H(tmp + I + q + uint16ToString(i) + uint8ToString(j) + D_ITER)
        hash.update(tmp)
    hash.update(D_PBLC)
    return hash.digest()

# Algorithm 4: Verifying a Signature and Message Using a Public Key
#
def lmots_verify_sig(public_key, sig, message):
    z = lmots_sig_to_pub(sig, message)
    # print "z: " + stringToHex(z)
    if z == public_key:
        return 1
    else:
        return 0


if __name__ == '__main__':
    import timeit
    # LM-OTS test functions
    #
    I = entropySource.read(31)
    q = uint32ToString(0)
    start = timeit.timeit()
    private_key = lmots_gen_priv()
    end = timeit.timeit()
    print("Priv Key Generation Time: {}".format(end-start))

    print "LMOTS private key: "
    for i, x in enumerate(private_key):
        print "x[" + str(i) + "]:\t" + stringToHex(x)

    start = timeit.timeit()
    public_key = lmots_gen_pub(private_key, I, q)
    end = timeit.timeit()
    print("Pub Key Generation Time: {}".format(end-start))

    print "LMOTS public key: "
    print stringToHex(public_key)

    message = "The right of the people to be secure in their persons, houses, papers, and effects, against unreasonable searches and seizures, shall not be violated, and no warrants shall issue, but upon probable cause, supported by oath or affirmation, and particularly describing the place to be searched, and the persons or things to be seized."

    print "message: " + message

    start = timeit.timeit()
    sig = lmots_gen_sig(private_key, I, q, message)
    end = timeit.timeit()
    print("Signature Creation Time: {}".format(end-start))

    print "LMOTS signature byte length: " + str(len(sig))

    print "LMOTS signature: "
    print_lmots_sig(sig)

    start = timeit.timeit()
    lmots_verify_sig(public_key,sig,message)
    end = timeit.timeit()
    print("Signature Verification Time: {}".format(end-start))

    print "verification: "
    print "true positive test: "
    if (lmots_verify_sig(public_key, sig, message) == 1):
        print "passed: message/signature pair is valid as expected"
    else:
        print "failed: message/signature pair is invalid"

    print "false positive test: "
    if (lmots_verify_sig(public_key, sig, "some other message") == 1):
        print "failed: message/signature pair is valid (expected failure)"
    else:
        print "passed: message/signature pair is invalid as expected"