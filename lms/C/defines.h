#ifndef DEFINES_H
#define DEFINES_H
// LMOTS
#define P_PARAM 34       // ?

// LMS
#define H_PARAM 5       // the height (number of levels - 1) in the tree
#define N_PARAM 32      // the number of bytes associated with each node
#define TWO_POWER_H 1024// 2^H
#define W_PARAM 8
#define T_FUNCT_LEN_LEAF 98     // 32 + 31 + 32 + 2 = 97 
                                // (pub[j-(1<<h)] + I + temp + D_LEAF)
#define T_FUNCT_LEN_INTR 130    // 32 + 32 + 31 + 32 + 2 = 129
                                // (T(2*j) + T(2*j+1) + I + temp + D_INTR)

#endif