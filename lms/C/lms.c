#include <stdlib.h>
#include <stdio.h>
#include "lms.h"

/*
extern enum lms_algorithm_type lms_algorithm_type_cur;// = lms_sha256_n32_h5;
lms_algorithm_type_cur = lms_sha256_n32_h5;
extern enum lmots_algorithm_type lmots_algorithm_type_cur;// = lmots_sha256_n32_w8;
lmots_algorithm_type_cur = lmots_sha256_n32_w8;
*/
/*
import hashlib
#from Crypto import Random
import os   # for urandom
from bitstring import BitArray, ConstBitStream
import binascii

#
# When concatenated with "+", each component is made to be a string
#  - These could be changed to BitArrays
# Needs to be tested with OTS sigs
#


# LMOTS constants
D_ITER = chr(0x00) # in the iterations of the LM-OTS algorithms
D_PBLC = chr(0x01) # when computing the hash of all of the iterates in the LM-OTS algorithm
D_MESG = chr(0x02) # when computing the hash of the message in the LMOTS algorithms
D_LEAF = chr(0x03) # when computing the hash of the leaf of an LMS tree
D_INTR = chr(0x04) # when computing the hash of an interior node of an LMS tree

NULL = chr(0) # used as padding for encoding

# LMOTS parameters
#n = 32; p = 34; w = 8; ls = 0

# Hash function
#def H2(value, m=40):
#    a = hashlib.sha256(value).digest()
#    #print "\n\naaaa\n\n"
#    #print a
#    return a



# LMOTS for testing
import sys
sys.path.append('../lm-ots')
from lm_ots import lmots_gen_priv, lmots_gen_pub, lmots_gen_sig, lmots_decode_sig, lmots_sig_to_pub
# Done LMOTS for testing
*/
char* H(char* msg)
{
    return "0123456789abcdef0123456789abcdef";
}

void uint8ToString(unsigned int x, char ret[1])
{
    char r[1] = {'0'};
    sprintf(r, "%d", x);
    ret = r;
}

void uint16ToString(unsigned int x, char ret[2])
{
    char r[2] = {'0', '0'};
    sprintf(r, "%d", x);
    ret = r;
}

void uint32ToString(unsigned int x, char ret[4])
{
    char r[4] = {'0', '0', '0', '0'};
    sprintf(r, "%d", x);
    ret = r;
}

void uintXToString(int x, char* ret)
{
    uint32ToString(x, ret);
}

int stringToUint(char* x)
{
    return atoi(x);
}

/*
def pick_uint_function(n):
    if n == 8:
        return uint8ToString
    elif n == 16:
        return uint16ToString
    else:   
        return uint32ToString
*/
/*
def pick_lms_algorithm_type(w, h):
    ret = lms_algorithm_type_enum[lms_reserved]
    if w == 32:
        if h == 20:
            ret = lms_algorithm_type_enum[lms_sha256_n32_h20]
        elif h == 10:
            ret = lms_algorithm_type_enum[lms_sha256_n32_h10]
        elif h == 5:
            ret = lms_algorithm_type_enum[lms_sha256_n32_h5]
    elif w == 16:
        if h == 20:
            ret = lms_algorithm_type_enum[lms_sha256_n16_h20]
        elif h == 10:
            ret = lms_algorithm_type_enum[lms_sha256_n16_h10]
        elif h == 5:
            ret = lms_algorithm_type_enum[lms_sha256_n16_h5]
    return ret
*/