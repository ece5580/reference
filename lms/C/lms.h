#ifndef LMS_H
#define LMS_H
/*
// LMOTS constants
const unsigned char D_ITER = 0x00; // in the iterations of the LM-OTS algorithms
const unsigned char D_PBLC = 0x01; // when computing the hash of all of the iterates in the LM-OTS algorithm
const unsigned char D_MESG = 0x02; // when computing the hash of the message in the LMOTS algorithms
const unsigned char D_LEAF = 0x03; // when computing the hash of the leaf of an LMS tree
const unsigned char D_INTR = 0x04; // when computing the hash of an interior node of an LMS tree
*/
#define D_ITER 0x00 // in the iterations of the LM-OTS algorithms
#define D_PBLC 0x01 // when computing the hash of all of the iterates in the LM-OTS algorithm
#define D_MESG 0x02 // when computing the hash of the message in the LMOTS algorithms
#define D_LEAF 0x03 // when computing the hash of the leaf of an LMS tree
#define D_INTR 0x04 // when computing the hash of an interior node of an LMS tree

//const unsigned char NULL = 0x00; // used as padding for encoding

/*
#lmots_sha256_n32_w8 = 0x08000008 # typecode for LM-OTS with n=32, w=8
#lms_sha256_n32_h10 = 0x02000002 # typecode for LMS with n=32, h=10
*/
enum lms_algorithm_type {
    lms_reserved = 0x00000000,
    lms_sha256_n32_h20 = 0x00000001,
    lms_sha256_n32_h10 = 0x00000002,
    lms_sha256_n32_h5 = 0x00000003,
    lms_sha256_n16_h20 = 0x00000004,
    lms_sha256_n16_h10 = 0x00000005,
    lms_sha256_n16_h5 = 0x00000006
};

enum lms_algorithm_type lms_algorithm_type_cur;// = lms_sha256_n32_h5;

enum lmots_algorithm_type {
    lmots_sha256_n32_w1,
    lmots_sha256_n32_w2,
    lmots_sha256_n32_w4,
    lmots_sha256_n32_w8,
    lmots_sha256_n16_w1,
    lmots_sha256_n16_w2,
    lmots_sha256_n16_w4,
    lmots_sha256_n16_w8
};

enum lmots_algorithm_type lmots_algorithm_type_cur;// = lmots_sha256_n32_w8;

/*
hash_algorithms = {
    "sha256":   hashlib.sha256,
    "md5":      hashlib.md5
}
*/
int h;

char* H(char* msg);
void uint8ToString(unsigned int x, char ret[1]);
void uint16ToString(unsigned int x, char ret[2]);
void uint32ToString(unsigned int x, char ret[4]);
void uintXToString(int x, char* ret);
int stringToUint(char* x);
char pick_uint_function(unsigned int n);
enum lms_algorithm_type pick_lms_algorithm_type(unsigned int w, unsigned int h);

#endif