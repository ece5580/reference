#include <stdlib.h>
#include <string.h>
#include "defines.h"
#include "lms.h"
#include "lmspriv.h"
#include "lmots.h"

/*
# LMS N-time signature functions
#
#def lms_bytes_in_sig(sig):
#    return bytes_in_lmots_sig() + h*n + 4

# Compute root and other nodes
# Each node is associated with an n-byte string. Ti computes the string for the rth (jth) node
# See section 5.2 for equation
# Tested: All the parts of nodes[j] are strings so they concatenate together
*/


char priv[TWO_POWER_H][N_PARAM];    // all LMS private keys
char pub[TWO_POWER_H][N_PARAM];     // all LMS public keys
char lms_pub_key[N_PARAM];
int I;
int leaf_num;
char nodes[TWO_POWER_H][N_PARAM];

// char ts[TWO_POWER_H][N_PARAM])
char* T(int j) 
{
    //print type(self.H)
    if (j >= (1<<h))
    {
        char temp[W_PARAM];
        uintXToString(j, temp);
        char str[T_FUNCT_LEN_LEAF];
        strcat(str, pub[j-(1<<h)]);
        uintXToString(I, temp);
        strcat(str, temp);
        strcat(str, temp);
        uintXToString(D_LEAF, temp);
        strcat(str, temp);
        for (int col=0; col<N_PARAM; col++)
            nodes[j][col] = str[col];             //H(pub[j-(1<<h)] + I + temp + D_LEAF);//.digest()
        return nodes[j];
    }
    else
    {
        char temp[W_PARAM];
        uintXToString(j, temp);
        char str[T_FUNCT_LEN_INTR];
        strcat(str, T(2*j));
        strcat(str, T(2*j+1));
        uintXToString(I, temp);
        strcat(str,temp);
        strcat(str, temp);
        uintXToString(D_INTR, temp);
        strcat(str, temp);
        for (int col=0; col<N_PARAM; col++)
            nodes[j][col] = str[col];             //H(T(2*j) + T(2*j+1) + I + temp + D_INTR);//.digest()
        return nodes[j];
    }
}

void lmsprivDefault()
{
    lmspriv(8, 5, "sha256", 32, "lmots_sha256_n32_w8");
}

void lmspriv(int w_inp, int h_inp, char* hash_inp, int n_inp, char* typ_inp)
{
    // LMOTS parameters
    //int w = w_inp;  //= 8;
    //int h = h_inp;  //= 5    # height (number of levels -1) in the tree
    //int H = hash_algorithms[hash_inp];
    //int n = n_inp; //= 32 # number of bytes associted with each node

    //uintXToString = pick_uint_function(n_inp);
    //lms_algorithm_type_cur = pick_lms_algorithm_type(w_inp, h_inp);
    //print "+++++ Initializing +++++"
    I = rand();             // 31 byte?? distinct identifier string
    //char priv[32];                // LMS private key
    //char pub[32];                   // LMS public key
    for (int q=0; q<(1 << h); q++)        // Create pub/priv LM-OTS key pairs. Leaves will contain the pub keys
    {
        char ots_priv[N_PARAM];
        char* str = lmots_gen_priv();
        for (int col=0; col<N_PARAM; col++)
            ots_priv[col] = str[col];
        char temp[W_PARAM];
        uintXToString(q, temp);
        char* ots_pub = lmots_gen_pub(ots_priv,  I,  temp);
        for (int col=0; col<N_PARAM; col++)
            priv[q][col] = ots_priv[col];
        for (int col=0; col<N_PARAM; col++)
            pub[q][col] = ots_pub[col];
        //print "h = " + str(q) + " / " + str(2**self.h)
    }
    leaf_num = 0;           // Next LM-OTS private key that has not yet been used
    //nodes = {}            // Nodes of the tree
    char* temp = T(1);      // PMS public key is the string consisting of a four-byte enumeration that identifies
                            // The parameters in use, followed by the string T[1]
    for (int col=0; col<N_PARAM; col++)
            lms_pub_key[col] = temp[col];
    //print "LMS public key: " + self.lms_pub_key
}

int num_sigs_remaining()
{
    return (1 << h) - leaf_num;
}


void printHex()
{
    /*
    for i, p in enumerate(self.priv):
        print "priv[" + str(i) + "]:"
        for j, x in enumerate(p):
            print "x[" + str(i) + "]:\t" + stringToHex(self.pub[i])
        print "pub[" + str(i) + "]:\t" + stringToHex(self.pub[i])
    for t, T in self.nodes.items():
        print "T(" + str(t) + "):\t" + stringToHex(T)
    print "pub: \t" + stringToHex(self.lms_pub_key)
    */
}
    
void get_pub_key(char key[N_PARAM])
{
    //return lms_pub_key;
    for (int col=0; col<N_PARAM; col++)
        lms_pub_key[col] = key[col];
}

void get_path(int leaf_num, char ret[H_PARAM][N_PARAM])
{
    int node_num = leaf_num + (1 << h);
    //char path[H_PARAM];
    int i = 0;
    for (int nn = node_num; nn>1; nn = nn/2)
    {
        if (node_num % 2)   // # If odd
        {
            for (int col=0; col<N_PARAM; col++)
                ret[i][col] = nodes[nn-1][col];
        }
        else
        {
            for (int col=0; col<N_PARAM; col++)
                ret[i][col] = nodes[nn+1][col];
        }
        i++;
    }
}

// Concatenate parts of the LMS sig
void lms_encode_sig(char lmots_sig[N_PARAM], char path[H_PARAM][N_PARAM], char res[N_PARAM])
{
    char temp[W_PARAM];
    uintXToString(lms_algorithm_type_cur, temp);
    strcat(res, temp);
    strcat(res, lmots_sig);
    //res = temp + lmots_sig;
    for (int i=0; i<H_PARAM; i++)
        //res = res + path[i];
        strcat(res, path[i]);
}

/*
# An LMS signature consists of (see section 5.3)
#  - a typecode indicating the particular LMS algorithm,
#  - an LM-OTS signature, and
#  - an array of values that is associated with the path through the tree from the leaf associated with the LM-OTS
#    signature to the root.
*/
int sign(char* message, char sig[N_PARAM])
{
    //#print "+++++ In sign +++++"
    //# Check if no keys remain
    if (leaf_num >= (1 << h))
        return 0;
    //# Compute LM-OTS signature of the message using leaf number of next LM-OTS priv key    
    //char temp[W_PARAM];
    //uintXToString(leaf_num, temp);
    sig = lmots_gen_sig(priv[leaf_num], I, leaf_num, message);
    //# Get the path of the leaf
    char path[H_PARAM][N_PARAM];
    get_path(leaf_num, path);
    //# ?
    //leaf_num = self.leaf_num
    //# Increment LMS priv key leaf number
    leaf_num = leaf_num + 1;
    //# Now the signature can be returned
    char retsig[N_PARAM];
    lms_encode_sig(sig, path, retsig);
    return 1;
}

