#ifndef LMSPRIV_H
#define LMSPRIV_H

/*
# LMS N-time signature functions
#
#def lms_bytes_in_sig(sig):
#    return bytes_in_lmots_sig() + h*n + 4
*/
char*/*[TwoToHEIGHT][LEN]*/ T(int j);//, char ts[TwoToHEIGHT][LEN]);
//list[TwoToHEIGHT][LEN] T(j);
void lmsprivDefault();
void lmspriv(int w_inp, int h_inp, char* hash_inp, int n_inp, char* typ_inp);
int num_sigs_remaining();
void printHex();
void get_pub_key(char key[N_PARAM]);
void get_path(int leaf_num, char ret[H_PARAM][N_PARAM]);    
void lms_encode_sig(char lmots_sig[N_PARAM], char path[H_PARAM][N_PARAM], char sig[N_PARAM]);
int sign(char* message, char sig[N_PARAM]);

#endif