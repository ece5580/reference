#include <string.h>
#include "defines.h"
#include "lmots.h"
#include "lms.h"
#include "lmspriv.h"

char value[N_PARAM];

void lmspub(char val[N_PARAM], int h_inp, int n_inp, int p_inp, char* hashalgo)
{
    for (int i=0; i<N_PARAM; i++)
        value[i] = val[i];  //# public key
    //h = h_inp;
    //n = n_inp;
    //p = p_inp;

    //uintXToString = pick_uint_function(n_inp);

    //H = hash_algorithms[hashalgo];
}


int bytes_in_lmots_sig()
{
    return N_PARAM*(P_PARAM+1)+40; //# 4 + n + 31 + 1 + 4 + n*p
}
/*
    # Break apart LMS sig
#    def lms_decode_sig(self, lmots_sig, path):
#        typecode = sig[0:4]
#        if typecode != self.uintXToString(lms_algorithm_type):
#            print "error decoding signature; got typecode " + stringToHex(typecode) + ", expected: " + stringToHex(self.uintXToString(lms_algorithm_type))
#            return ""
#        pos = 4 + self.bytes_in_lmots_sig()
#        lmots_sig = sig[4:pos]
#        path = []
#        for i in range(0,self.h):
#            path.append(sig[pos:pos+self.n])
#            pos = pos + self.n
#        return lmots_sig, path
*/
int lms_decode_sig(char sig[N_PARAM], char lmots_sig[N_PARAM], char path[H_PARAM][N_PARAM])
{
    char typecode[5];
    for (int col=0; col<5; col++)
        typecode[col] = sig[col];
    char tmp[N_PARAM];      // ??
    uintXToString(lms_algorithm_type_cur, tmp);
    if (strcmp(typecode, tmp) != 0)
        //print "error decoding signature; got typecode " + stringToHex(typecode) + ", expected: " + stringToHex(self.uintXToString(lms_algorithm_type))
        return 0;   // Failure
    int pos = 4 + bytes_in_lmots_sig();
    for (int col=4; col<pos; col++)
        lmots_sig[col-4] = sig[col];
    //path = list();
    for (int i=0; i<H_PARAM; i++)
    {
        //# print "sig[" + str(i) + "]:\t" + stringToHex(sig[pos:pos+n])
        int col2 = pos;
        for (int col=0; col<N_PARAM; col++)
        {
            path[i][col] = sig[col2];
            col2++; // pos to pos+n
        }
        pos = pos + N_PARAM;
    }
    //return lmots_sig
    return 1;   // Success
}

void lms_print_sig(char sig[N_PARAM])
{
    //#print len(sig)
    //lmots_sig, path = self.lms_decode_sig(sig)
    //#print_lmots_sig(lmots_sig)
    //#print lmots_sig
    //#print len(lmots_sig)
    //for i, e in enumerate(path):
    //    print "path[" + str(i) + "]:\t" + str(stringToHex(e))
}

int lms_verify_sig(char* message, char sig[N_PARAM])
{
    char lmots_sig[N_PARAM];
    char path[H_PARAM][N_PARAM];
    int ret = lms_decode_sig(sig, lmots_sig, path);
    //C, I, q, y = lmots_decode_sig(lmots_sig);    //# returns: randomizer, identifier string, diversification string, y
    char C[N_PARAM];    // n-byte
    int I;              // 31 bytes
    char q[8];          // 4 byte string
    char y[TWO_POWER_H];// ??
    lmots_decode_sig(lmots_sig, C, I, q, y);    //# returns: randomizer, identifier string, diversification string, y
    //# Assign value of public key to associated leaf on LMS tree
    int node_num = (1 << h) + stringToUint(q);                  //# node number (leaf_number = q)
    //# Use LM-OTS sig verification algo to compute LM-OTS public key from LM-OTS sig and message
    char tmp[N_PARAM];
    lmots_sig_to_pub(lmots_sig, message, tmp);  //# candidate public key computed from LM-OTS signature and message
    
    //char tmp[N_PARAM];
    char tmp2[200];     // ??
    // tmp
    strcat(tmp2, tmp);
    // I
    char* tmp3;
    uintXToString(I, tmp3);
    strcat(tmp2, tmp3);
    // node_num
    char tmp4[W_PARAM];
    uintXToString(node_num/2, tmp4);
    strcat(tmp2, tmp4);
    // D_LEAF
    char tmp5[W_PARAM];
    uintXToString(D_LEAF, tmp5);
    strcat(tmp2, tmp5);
    // Hash
    char* tmpHash = H(tmp2);
    for (int col=0; col<N_PARAM; col++)
        tmp[col] = tmpHash[col];
    //tmp = H(tmp + I + self.uintXToString(node_num) + D_LEAF);//.digest()
    //# Compute root of tree from leaf value and node array (path[])
    int i = 0;
    while (node_num > 1)
    {
        if (node_num%2 == 1)
        {
            char tmp2[200];
            // Path[i]
            strcat(tmp2, path[i]);
            // tmp
            strcat(tmp2, tmp);
            // I
            char* tmp3;
            uintXToString(I, tmp3);
            strcat(tmp2, tmp3);
            // node_num/2
            char tmp4[W_PARAM];
            uintXToString(node_num/2, tmp4);
            strcat(tmp2, tmp4);
            // D_INTR
            char tmp5[W_PARAM];
            uintXToString(D_INTR, tmp5);
            strcat(tmp2, tmp5);
            // Hash
            char* tmpHash = H(tmp2);
            for (int col=0; col<N_PARAM; col++)
                tmp[col] = tmpHash[col];
            //tmp = H(path[i] + tmp + I + uintXToString(node_num/2) + D_INTR);//.digest()
        }
        else
        {
            char tmp2[200];
            // tmp
            strcat(tmp2, tmp);
            // Path[i]
            strcat(tmp2, path[i]);
            // I
            char* tmp3;
            uintXToString(I, tmp3);
            strcat(tmp2, tmp3);
            // node_num/2
            char tmp4[W_PARAM];
            uintXToString(node_num/2, tmp4);
            strcat(tmp2, tmp4);
            // D_INTR
            char tmp5[W_PARAM];
            uintXToString(D_INTR, tmp5);
            strcat(tmp2, tmp5);
            // Hash
            char* tmpHash = H(tmp2);
            for (int col=0; col<N_PARAM; col++)
                tmp[col] = tmpHash[col];
        }
        node_num = node_num/2;
        i = i+1;
    }
    //# Check if root value matches public key
    if (tmp == value)
        return 1;    //# message/signature pair is valid
    else
        return 0;    //# message/signature pair is invalid
}