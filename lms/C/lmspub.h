#ifndef LMSPUB_H
#define LMSPUB_H

#define LEN 32

/*
external char value;
external int h;
external int n;
external int p;
*/

void lmspub(char* value, int h_inp, int n_inp, int p_inp, char* hashalgo);
int bytes_in_lmots_sig();
int lms_decode_sig(char sig[N_PARAM], char lmots_sig[N_PARAM], char path[H_PARAM][N_PARAM]);
void lms_print_sig(char sig[N_PARAM]);
int lms_verify_sig(char* message, char sig[LEN]);
char* stringToHex(char* x);

#endif