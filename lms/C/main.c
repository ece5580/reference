#include <stdio.h>
#include "defines.h"
#include "lms.h"
#include "lmspriv.h"
#include "lmspub.h"

void main()
{
    char* message = "Some cool message about snow and basketball and lots of other interesting topics.";

    printf("\n\n+++++ LMS Test +++++");

    //# Generate lms priv and pub keys
    int w = 8;
    int h = 5;
    int n = 32;
    int p = 34;  //#??? WHy?
    char* hsh = "sha256";
    char* lmots = "lmots_sha256_n32_w8";
    lmspriv(w, h, hsh, n, lmots);
    printf(" - Private key generated");
    char tmpKey[N_PARAM];
    get_pub_key(tmpKey);
    lmspub(tmpKey, h, n, p, hsh);
    printf(" - Public key generated");

    printHex();

    //# Use all the sigs
    for (int i=0; i<(1<<H_PARAM); i++)
    {
        char sig[N_PARAM];
        sign(message, sig);
        printf("LMS signature byte length: %d", sizeof(sig));
        lms_print_sig(sig);

        //# Test the sig (--> Need OTS working to test)
        //#"""
        printf("true positive test");
        if (lms_verify_sig(message, sig) == 1)
            printf("passed: LMS message/signature pair is valid");
        else
            printf("failed: LMS message/signature pair is invalid");

        printf("false positive test");
        if (lms_verify_sig("other message", sig) == 1)
            printf("failed: LMS message/signature pair is valid (expected failure)");
        else
            printf("passed: LMS message/signature pair is invalid as expected");
        //#"""
    }
}