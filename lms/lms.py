import hashlib
#from Crypto import Random
import os   # for urandom
from bitstring import BitArray, ConstBitStream
import binascii

#
# When concatenated with "+", each component is made to be a string
#  - These could be changed to BitArrays
# Needs to be tested with OTS sigs
#


# LMOTS constants
D_ITER = chr(0x00) # in the iterations of the LM-OTS algorithms
D_PBLC = chr(0x01) # when computing the hash of all of the iterates in the LM-OTS algorithm
D_MESG = chr(0x02) # when computing the hash of the message in the LMOTS algorithms
D_LEAF = chr(0x03) # when computing the hash of the leaf of an LMS tree
D_INTR = chr(0x04) # when computing the hash of an interior node of an LMS tree

NULL = chr(0) # used as padding for encoding

lmots_sha256_n32_w8 = 0x08000008 # typecode for LM-OTS with n=32, w=8
lms_sha256_n32_h10 = 0x02000002 # typecode for LMS with n=32, h=10
#enum lms_algorithm_type {
#lms_reserved = 0x00000000,
#lms_sha256_n32_h20 = 0x00000001,
#lms_sha256_n32_h10 = 0x00000002,
lms_sha256_n32_h5 = 0x00000003
#lms_sha256_n16_h20 = 0x00000004,
#lms_sha256_n16_h10 = 0x00000005,
#lms_sha256_n16_h5 = 0x00000006
#};

# LMOTS parameters
n = 32; p = 34; w = 8; ls = 0

#LMOTS for testing
# def lmots_gen_priv():
#     x = []
#     for i in range(p):
#         x.append(os.urandom(n))
#     return x
# def lmots_gen_pub(x, I, q):
#     return H(I + q + ''.join(x) + D_PBLC)
# def lmots_gen_sig(C, I, q, y):
#     result = uint32str(lmots_sha256_n32_w8) + "".join(C) + I + NULL + q
#     for i, e in enumerate(y):
#         result = result + y[i]
#     return result
def H2(value, m=40):
    a = hashlib.sha256(value).digest()
    #print "\n\naaaa\n\n"
    #print a
    return a
# def lmots_sig_to_pub(lmots_sig, message):
#     ots_priv = lmots_gen_priv()
#     return lmots_gen_pub(ots_priv, self.I, uint32str(q))
def bytes_in_lmots_sig():
    return n*(p+1)+40 # 4 + n + 31 + 1 + 4 + n*p
import sys
sys.path.append('../lm-ots')
from lmots import lmots_gen_priv, lmots_gen_pub, lmots_gen_sig, lmots_decode_sig, lmots_sig_to_pub
# Done LMOTS for testing

def uint32str(x):
    c4 = chr(x & 0xff)
    x = x >> 8
    c3 = chr(x & 0xff)
    x = x >> 8
    c2 = chr(x & 0xff)
    x = x >> 8
    c1 = chr(x & 0xff)
    return c1 + c2 + c3 + c4

def stringToUint(x):
    sum = 0
    for c in x:
        sum = sum * 256 + ord(c)
    return sum

#
# LMS N-time signature functions
#

h = 5
#h = 10  # height (number of levels -1) in the tree
#n = # number of bytes associted with each node

# Concatenate parts of the LMS sig
def lms_encode_sig(lmots_sig, path):
    #print "+++++ In lms_encode_sig +++++"
    #print lmots_sig
    #print path
    res = uint32str(lms_sha256_n32_h5) + lmots_sig
    for i, e in enumerate(path):
        res = res + path[i]
    #print "res--------------------------->"
    #print len(res)
    #print "+++++ End lms_encode_sig +++++"
    return res

# Break apart LMS sig
def lms_decode_sig(lmots_sig, path):
    typecode = sig[0:4]
    if typecode != uint32str(lms_sha256_n32_h5):
        print "error decoding signature; got typecode " + stringToHex(typecode) + ", expected: " + stringToHex(uint32str(lms_sha256_h5))
        return ""
    pos = 4 + bytes_in_lmots_sig()
    lmots_sig = sig[4:pos]
    path = []
    for i in range(0,h):
        path.append(sig[pos:pos+n])
        pos = pos + n
    return lmots_sig, path

def lms_decode_sig(sig):
    typecode = sig[0:4]
    if (typecode != uint32str(lms_sha256_n32_h5)):
        print "error decoding signature; got typecode " + stringToHex(typecode) + ", expected: " + stringToHex(uint32ToString(lms_sha256_h5))
        return ""
    pos = 4 + bytes_in_lmots_sig()
    lmots_sig = sig[4:pos]
    path = list()
    for i in range(0,h):
        # print "sig[" + str(i) + "]:\t" + stringToHex(sig[pos:pos+n])
        path.append(sig[pos:pos+n])
        pos = pos + n
    return lmots_sig, path

def lms_print_sig(sig):
    #print len(sig)
    lmots_sig, path = lms_decode_sig(sig)
    #print_lmots_sig(lmots_sig)
    #print lmots_sig
    #print len(lmots_sig)
    for i, e in enumerate(path):
        print "path[" + str(i) + "]:\t" + str(stringToHex(e))
        
def lms_bytes_in_sig(sig):
    return bytes_in_lmots_sig() + h*n + 4
    
class lms_priv_key(object):
    
    # Compute root and other nodes
    # Each node is associated with an n-byte string. Ti computes the string for the rth (jth) node
    # See section 5.2 for equation
    # Tested: All the parts of nodes[j] are strings so they concatenate together
    def T(self, j):
        #print "+++++ In T +++++"
        if j >= 2**h:
            #print len(self.pub[j-2**h])
            #print len(self.I)
            #print len(uint32str(j))
            #print len(D_LEAF)
            self.nodes[j] = H2(self.pub[j-2**h] + self.I + uint32str(j) + D_LEAF)
            #print len(self.nodes[j])
            #print self.nodes[j]
            #print "+++++ End T1 +++++"
            return self.nodes[j]
        else:
            #print len(self.T(2*j))
            #print len(self.T(2*j+1))
            #print len(self.I)
            #print len(uint32str(j))
            #print len(D_INTR)
            self.nodes[j] = H2(self.T(2*j) + self.T(2*j+1) + self.I + uint32str(j) + D_INTR)
            #print len(self.nodes[j])
            #print self.nodes[j]
            #print "+++++ End T2 +++++"
            return self.nodes[j]
    
    def __init__(self): #, n, p, w, ls):
        # LMOTS parameters
        #self.n = n #= 32;
        #self.p = p #= 34;
        #self.w = w #= 8;
        #self.ls = ls #= 0
        print "+++++ Initializing +++++"
        self.I = os.urandom(31)         # 31 byte disticnt identifier string
        self.priv = []                  # LMS private key
        self.pub = []                   # LMS public key
        for q in range(0, 2**h):        # Create pub/priv LM-OTS key pairs. Leaves will contain the pub keys
            ots_priv = lmots_gen_priv()
            ots_privH = []
            #print "Private"
            for i in range(len(ots_priv)):
                ots_privH.append(BitArray(bytes=ots_priv[i]))
            #print ots_priv
            #print self.I
            #print BitArray(bytes=self.I)
            ots_pub = lmots_gen_pub(ots_privH,  BitArray(bytes=self.I),  BitArray(bytes=uint32str(q)))
            #print "Public"
            #print ots_pub
            self.priv.append(ots_priv)
            self.pub.append(ots_pub)
            print "h = " + str(q) + " / " + str(2**h)
        self.leaf_num = 0               # Next LM-OTS private key that has not yet been used
        self.nodes = {}                 # Nodes of the tree
        self.lms_pub_key = self.T(1)    # PMS public key is the string consisting of a four-byte enumeration that identifies
                                        # The parameters in use, followed by the string T[1]
        #print type(self.lms_pub_key)
        #print len(self.lms_pub_key)
        print "LMS public key: " + self.lms_pub_key
        #print "+++++ End __init__ +++++"
    
    def num_sigs_remaining():
        return 2**h - self.leaf_num
    
    def printHex(self):
        for i, p in enumerate(self.priv):
            print "priv[" + str(i) + "]:"
            for j, x in enumerate(p):
                print "x[" + str(i) + "]:\t" + stringToHex(self.pub[i])
            print "pub[" + str(i) + "]:\t" + stringToHex(self.pub[i])
        for t, T in self.nodes.items():
            print "T(" + str(t) + "):\t" + stringToHex(T)
        print "pub: \t" + stringToHex(self.lms_pub_key)
    
    def get_pub_key(self):
        return self.lms_pub_key
    
    def get_path(self, leaf_num):
        node_num = leaf_num + 2**h
        path = []
        while node_num > 1:
            if node_num % 2:    # If odd
                path.append(self.nodes[node_num -1])
            else:
                path.append(self.nodes[node_num + 1])
            node_num = node_num/2
        return path
    
    # An LMS signature consists of (see section 5.3)
    #  - a typecode indicating the particular LMS algorithm,
    #  - an LM-OTS signature, and
    #  - an array of values that is associated with the path through the tree from the leaf associated with the LM-OTS
    #    signature to the root.
    def sign(self, message):
        #print "+++++ In sign +++++"
        # Check if no keys remain
        if self.leaf_num >= 2**h:
            return ""
        # Compute LM-OTS signature of the message using leaf number of next LM-OTS priv key
        tmp = []
        for i in range(len(self.priv[self.leaf_num])):
            tmp.append(BitArray(bytes=self.priv[self.leaf_num][i]))
        sig = lmots_gen_sig(tmp, BitArray(bytes=self.I), BitArray(bytes=uint32str(self.leaf_num)), BitArray(hex=binascii.hexlify(message)))
        #print sig
        # Get the path of the leaf
        path = self.get_path(self.leaf_num)
        # ?
        leaf_num = self.leaf_num
        # Increment LMS priv key leaf number
        self.leaf_num = self.leaf_num + 1
        # Now the signature can be returned
        return lms_encode_sig(sig, path)
        #print "+++++ End sign +++++"


H = hashlib.sha256

class lms_pub_key(object):
    def __init__(self, value):
        self.value = value  # public key
    
    def lms_verify_sig(self, message, sig):
        lmots_sig, path = lms_decode_sig(sig)
        C, I, q, y = lmots_decode_sig(BitArray(bytes=lmots_sig))    # returns: randomizer, identifier string, diversification string, y
        # Assign value of public key to associated leaf on LMS tree
        node_num = 2**h + stringToUint(q)                  # node number (leaf_number = q)
        # Use LM-OTS sig verification algo to compute LM-OTS public key from LM-OTS sig and message
        tmp = lmots_sig_to_pub(BitArray(bytes=lmots_sig), BitArray(hex=binascii.hexlify(message)))  # candidate public key computed from LM-OTS signature and message
        tmp = H2(tmp + I + uint32str(node_num) + D_LEAF)
        # Compute root of tree from leaf value and node array (path[])
        i = 0
        while node_num > 1:
            if node_num%2 == 1:
                tmp = H2(path[i] + tmp + I + uint32str(node_num/2) + D_INTR)
            else:
                tmp = H2(tmp + path[i] + I + uint32str(node_num/2) + D_INTR)
            node_num = node_num/2
            i = i+1
        # Check if root value matches public key
        if tmp == self.value:
            return 1    # message/signature pair is valid
        else:
            return 0    # message/signature pair is invalid

"""
# From Appendix B
def lms_compute_pub_key():
    I
    r
    D_LEAF
    data_stack = []
    int_stack = []
    for i in range(0, num_lmots_keys, 2):
        level = 0
        for j in range(0, 1):
            r = node_numb   #?
            data_stack.append(H(OTS_PUBKEY[i+j] + I + uint32str(r) + D_LEAF))
            int_stack.append(level)
        while len(int_stack) >= 2:
            if int_stack[-1] == int_stack[-2]:   # level of top 2 elements on integer stack are equal
                hash_init()    #?
                siblings = ""
                for k in range(0, 1):
                    siblings = data_stack.pop() + siblings
                    level = int_stack.pop()
                hash_update(siblings)   #?
                r = node_numb    #?
                hash_update(I + uint32str(r) + D_INTR)
                data_stack.append(hash_final()) #?
                int_stack.append(level + 1)
    public_key = data_stack.pop()
    print(public_key)
    return public_key
"""

# Test LMS signatures
def stringToHex(x):
    return "".join("{:02x}".format(ord(c)) for c in x)

message = "Some cool message about snow and basketball and lots of other interesting topics."

print "\n\n+++++ LMS Test +++++"

# Generate lms priv and pub keys
lms_priv = lms_priv_key()
print " - Private key generated"
lms_pub = lms_pub_key(lms_priv.get_pub_key())
print " - Public key generated"

lms_priv.printHex()

# Use all the sigs
for i in range(0, 5): #2**h):
    sig = lms_priv.sign(message)
    print "LMS signature byte length: " + str(len(sig))
    lms_print_sig(sig)
    
    # Test the sig (--> Need OTS working to test)
    #"""
    print "true positive test"
    if (lms_pub.lms_verify_sig(message, sig) == 1):
        print "passed: LMS message/signature pair is valid"
    else:
        print "failed: LMS message/signature pair is invalid"
    
    print "false positive test"
    if (lms_pub.lms_verify_sig("other message", sig) == 1):
        print "failed: LMS message/signature pair is valid (expected failure)"
    else:
        print "passed: LMS message/signature pair is invalid as expected"
    #"""
