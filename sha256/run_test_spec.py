# -*- coding: utf-8 -*-
#!/usr/bin/env python

"""
Created on Tue Feb 09 15:42:36 2016

@author: kazeitz
"""
import subprocess, sys

total = 0
sums = 0

with open("test_spec_output_1000.txt", "w+") as output:
    for x in range(0, 1000):
        total = total+1
        pipe = subprocess.Popen(["python", "./test_spec.py"], stdout=subprocess.PIPE,stderr=output);
        out = pipe.communicate() 
        f = out[0]
        f = float(f)
        sums = sums+f
 
    avg = sums/total
         
    output.write("\n###########################################################\n")
    output.write("The average run time for the four tests was: ")
    output.write(str(avg))
    output.write("\n###########################################################")