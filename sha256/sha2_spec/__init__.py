#!/usr/bin/python

"""__init__.py: This is the init for the executable specification version of 
sha256 for Group 8 and our project selection of Hash-Based Signature Systems 
and the MSP-430.  """

__author__ = "Kimberly Zeitz"
__credits__ = ["Kimberly Zeitz", "Michael Cantrell", "Caleb Stroud", "Group 8"]
__license__ = ""
__version__ = "1.0.0"
__maintainer__ = "Kimberly Zeitz"
__email__ = "kazeitz@vt.edu"
__status__ = "Executable Specification"

#==============================================================================
# This code was adapted from:
# __author__ = 'Thomas Dixon'
# __license__ = 'MIT'
# https://github.com/thomdixon/pysha2/blob/master/sha2/__init__.py
#==============================================================================

from sha256_spec import sha256_spec
