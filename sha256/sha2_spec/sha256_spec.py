# -*- coding: utf-8 -*-
#!/usr/bin/env python

"""sha256.py: This is the executable specification version of sha256 for 
Group 8 and our project selection of Hash-Based Signature Systems and the 
MSP-430.  This will serve as a reference model to study how the cryptographic 
algorithm works, and to generate test-vectors."""

__author__ = "Kimberly Zeitz"
__credits__ = ["Kimberly Zeitz", "Michael Cantrell", "Caleb Stroud", "Group 8"]
__license__ = ""
__version__ = "1.0.0"
__maintainer__ = "Kimberly Zeitz"
__email__ = "kazeitz@vt.edu"
__status__ = "Executable Specification"

#==============================================================================
# As stated in NIST FIPS 180-4: "SHA-256 may be used to hash a message, M, 
# having a length of 'l' bits where 0 <= l <= 2^64.  This algorithm uses
# 1. A message schedule of sixty-four 32-bit words
# 2. Eight working variables of 32 bits each
# 3. A hash value of eight 32-bit words
#
# The final result of SHA-256 is a 256-bit message digest
#
# Use: import sha2_spec
#      sha2_spec.sha256_spec('Message Here').hexdigest()
#
# Test: python test.py
#
# This code was adapted from:
# __author__ = 'Thomas Dixon'
# __license__ = 'MIT'
# https://github.com/thomdixon/pysha2/blob/master/sha2/sha256.py
#==============================================================================

import copy, struct, sys

#==============================================================================
# Constructor
#==============================================================================
def new(m=None):
    return sha256_spec(m)
        
class sha256_spec(object):
    
#==============================================================================
# Initialize the eight hash values to be the first 32 bits of the fractional 
# parts of the square roots of the first eight primes including 2 through 19
# as 32-bit words in hexadecimal
#==============================================================================
    
    _h = (0x6a09e667L, 0xbb67ae85L, 0x3c6ef372L, 0xa54ff53aL,
          0x510e527fL, 0x9b05688cL, 0x1f83d9abL, 0x5be0cd19L)
    
#==============================================================================
# Initialize the struct of round constants to be the first 32 bits of the 
# fractional parts of the square roots of the first sixty four primes including
# 2 through 311 as 32-bit words in hexadecimal
#==============================================================================
    
    _k = (0x428a2f98L, 0x71374491L, 0xb5c0fbcfL, 0xe9b5dba5L, 0x3956c25bL,
         0x59f111f1L, 0x923f82a4L, 0xab1c5ed5L, 0xd807aa98L, 0x12835b01L, 
         0x243185beL, 0x550c7dc3L, 0x72be5d74L, 0x80deb1feL, 0x9bdc06a7L, 
         0xc19bf174L, 0xe49b69c1L, 0xefbe4786L, 0x0fc19dc6L, 0x240ca1ccL, 
         0x2de92c6fL, 0x4a7484aaL, 0x5cb0a9dcL, 0x76f988daL, 0x983e5152L, 
         0xa831c66dL, 0xb00327c8L, 0xbf597fc7L, 0xc6e00bf3L, 0xd5a79147L,
         0x06ca6351L, 0x14292967L, 0x27b70a85L, 0x2e1b2138L, 0x4d2c6dfcL, 
         0x53380d13L, 0x650a7354L, 0x766a0abbL, 0x81c2c92eL, 0x92722c85L, 
         0xa2bfe8a1L, 0xa81a664bL, 0xc24b8b70L, 0xc76c51a3L, 0xd192e819L, 
         0xd6990624L, 0xf40e3585L, 0x106aa070L, 0x19a4c116L, 0x1e376c08L, 
         0x2748774cL, 0x34b0bcb5L, 0x391c0cb3L, 0x4ed8aa4aL, 0x5b9cca4fL, 
         0x682e6ff3L, 0x748f82eeL, 0x78a5636fL, 0x84c87814L, 0x8cc70208L,
         0x90befffaL, 0xa4506cebL, 0xbef9a3f7L, 0xc67178f2L)

    _output_size = 8
    
    blocksize = 1
    block_size = 64
    digest_size = 32
#==============================================================================
# Define function for sha256.py and check message input
#==============================================================================
    def __init__(self, m=None):        
        self._buffer = ''
        self._counter = 0
        
        if m is not None:
            if type(m) is not str:
                raise TypeError, '%s() argument 1 must be string, not %s' % (self.__class__.__name__, type(m).__name__)
            self.update(m)
            
#==============================================================================
# Binary Rotation Right
#==============================================================================
    def _rotr(self, x, y):
        return ((x >> y) | (x << (32-y))) & 0xFFFFFFFFL
     
#==============================================================================
# The message is processed in N 512 bit blocks, M^(1) - M^(N).
# Since the 512 bits of the input block can be expressed as sixteen 32-bit 
# words, the first 32 bits of message block i are denoted (M_0)^i, the next 32
# bits (M_1)^i, and so on to (M_15)^i.
# For each block a 64 entry message schedule array w[0-63] of 32 bit words
# is created.  The first 16 words of the piece of the message are copied into 
# the array, so w[0-15]. 
#   
# The first 16 words are then extended into the remaining 48 words, 
# w[16 ... 63] of the message schedule array     
#
# Working variables are set to the current hash value
#
# This includes the main loop of the compression function
#     
# The compressed piece of the message is added to the current hash value.  
#
# The final hash value in big-endian is produced by appending the hash values
#
# These are the six logical functions which operate on 32-bit words represented
# as x, y, and z.  Each function results in a new 32-bit word.
#==============================================================================
    def _sha256_spec_process(self, c):
       
        # 64 entry message schedule array
        w = [0]*64
        
        # Copy first 16 words of the piece of the message 
        # NIST FIPS 180-4 prepare the message schedule, {W_t} 0<=t<=15
        # ! is network, big-endian byte order
        # L is integer of size 16
        w[0:15] = struct.unpack('!16L', c)
         
        # The first 16 words are extended into the remaining 48 words 
        # NIST FIPS 180-4 prepare the message schedule, {W_t} 16<=t<=63
        # rotr = right rotate
        # ^ = xor
        # >> = right shift
        for i in range(16, 64):
            
            # NIST FIPS 180-4 Function (4.6) 
            s0 = self._rotr(w[i-15], 7) ^ self._rotr(w[i-15], 18) ^ (w[i-15] >> 3)
            
            # NIST FIPS 180-4 Function (4.7) 
            s1 = self._rotr(w[i-2], 17) ^ self._rotr(w[i-2], 19) ^ (w[i-2] >> 10)
            w[i] = (w[i-16] + s0 + w[i-7] + s1) & 0xFFFFFFFFL
         
        # Assign initial hash values to eight working variables
        a,b,c,d,e,f,g,h = self._h
         
        # Main loop of the compression function
        for i in range(64):
            # NIST FIPS 180-4 Function (4.4)
            s0 = self._rotr(a, 2) ^ self._rotr(a, 13) ^ self._rotr(a, 22)

            # NIST FIPS 180-4 Function (4.3)
            maj = (a & b) ^ (a & c) ^ (b & c)
            
            # NIST FIPS 180-4 temporary word 2             
            t2 = s0 + maj
            
            # NIST FIPS 180-4 Function (4.5)           
            s1 = self._rotr(e, 6) ^ self._rotr(e, 11) ^ self._rotr(e, 25)
            
            # NIST FIPS 180-4 Function (4.2)            
            ch = (e & f) ^ ((~e) & g)
           
           # NIST FIPS 180-4 Function temporary word 1           
            t1 = h + s1 + ch + self._k[i] + w[i]
             
            h = g
            g = f
            f = e
            e = (d + t1) & 0xFFFFFFFFL
            d = c
            c = b
            b = a
            a = (t1 + t2) & 0xFFFFFFFFL
            
        #Final hash of appended values     
        self._h = [(x+y) & 0xFFFFFFFFL for x,y in zip(self._h, [a,b,c,d,e,f,g,h])]
    
#==============================================================================
#  Gives the next piece of the message to continue hashing
#==============================================================================
    def update(self, m):
        if not m:
            return
        if type(m) is not str:
            raise TypeError, '%s() argument 1 must be string, not %s' % (sys._getframe().f_code.co_name, type(m).__name__)

        # Adds next piece of message to buffer
        self._buffer += m

        # Adds message length to counter
        self._counter += len(m)
        
        # Continue through each message block until the last 64 words
        # (This is 512 bit blocks)
        while len(self._buffer) >= 64:
            
            # Process the blocks
            self._sha256_spec_process(self._buffer[:64])
            self._buffer = self._buffer[64:]
        
#==============================================================================
# This is the preprocessing. As stated in NIST FIPS 180-4, the following steps
# are taken given the length of the message, M, is l bits.
# 1. Append the bit "1" to the end of the message
# 2. Pad the message with k zero bits such that k is the smallest, non-negative
#    solution to l + 1 + k equivalent to 448 mod 512
#    (The resulting message length modulo 512 in bits is 448)
# 3. Append the length of the message in bits, l, using a binary representation
#   (Makes entire padded message length multiple of 512 bits)
#
# Then the update function is called to update the message length and start 
# the processing
#==============================================================================
    def digest(self):
        # The message digest length
        # Original message length bitwise and with 0x3F which is 63 111111
        # Only take a 32-bit message
        # Ex: 12 11000000 AND 00111111 = 00000000 
        mdi = self._counter & 0x3F
        
        # ! is network, big-endian byte order
        # Q is integer of standard size 8 (So,'abc' is 8x3 = 24 in length)
        # Shift to left 3 places to concatenate the 1 to the end
        length = struct.pack('!Q', self._counter<<3)
        
        # Computing the padding (Number of zeros to add) 
        if mdi < 56:
            padlen = 55-mdi
        else:
            padlen = 119-mdi
        
        # Make a copy of current state
        r = self.copy()
        
        # \x80 = utf-8
        # Append the padding zeros
        # Append the 64 bit block that is equal to the number l (length)
        # expressed using binary representation
        # Give this preprocessed message to update and call the other processes
        r.update('\x80'+('\x00'*padlen)+length)
        
        # ! is network, big-endian byte order
        # L is integer of standard size 4
        # The final hash value in big-endian is produced by appending the 
        # hash values
        return ''.join([struct.pack('!L', i) for i in r._h[:self._output_size]])
        
#==============================================================================
# Returns the printable digest of the message that has been hashed   
#==============================================================================
    def hexdigest(self):
        return self.digest().encode('hex')

#==============================================================================
# Return a copy of the hash object with the same internal state as the 
# original hash object. This can be used to efficiently compute the digests 
# of strings that share a common initial substring.  A deep copy constructs a 
# new compound object and then, recursively, inserts copies, not references,
# into it of the objects found in the original      
#==============================================================================
    def copy(self):
        return copy.deepcopy(self)
    
    
    
# %%