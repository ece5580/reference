#! /usr/bin/env python

"""test_spec.py: This is the test file for the executable specification version
of sha256 for Group 8 and our project selection of Hash-Based Signature Systems 
and the MSP-430.  """

__author__ = "Kimberly Zeitz"
__credits__ = ["Kimberly Zeitz", "Michael Cantrell", "Caleb Stroud", "Group 8"]
__license__ = ""
__version__ = "1.0.0"
__maintainer__ = "Kimberly Zeitz"
__email__ = "kazeitz@vt.edu"
__status__ = "Executable Specification"

#==============================================================================
# This code was adapted from:
# __author__ = 'Thomas Dixon'
# __license__ = 'MIT'
# https://github.com/thomdixon/pysha2/blob/master/test.py
# 
# # Test: python test.py
#==============================================================================
import unittest, time
from sha2_spec import *

class TestSHA256(unittest.TestCase):
    def setUp(self):
        self.f = sha256_spec

    def test_empty(self):
        self.assertEqual(self.f('').hexdigest(),
                         'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855')

    def test_less_than_block_length(self):
        self.assertEqual(self.f('abc').hexdigest(),
                         'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad')

    def test_block_length(self):
        self.assertEqual(self.f('a'*64).hexdigest(),
                         'ffe054fe7ae0cb6dc65c3af9b61d5209f439851db43d0ba5997337df154668eb')

    def test_several_blocks(self):
        self.assertEqual(self.f('a'*1000000).hexdigest(),
                         'cdc76e5c9914fb9281a1c7e284d73e67f1809a48a497200e046d39ccc7112cd0')

if __name__ == '__main__':
    
    start = time.time()
    
    sha256_suite = unittest.TestLoader().loadTestsFromTestCase(TestSHA256)

    all_tests = unittest.TestSuite([sha256_suite])

    unittest.TextTestRunner(verbosity=2).run(all_tests)

    end = time.time()
    duration = end - start
    
    print duration